import React, {Component} from 'react';
import Cell from "./Cell";
import axios from 'axios'

const styledButton = {
    "display": "flex",
    "justifyContent": "center",
    "textAlign": "center"
}

const defaultGrid = [
    [0, 1, 0],
    [0, 1, 1],
    [1, 1, 0]
]

class Grid extends Component {
    state = {
        grid: defaultGrid
    }

    async componentDidMount() {
        const response = await axios.get('http://localhost:5000/get_initial_grid')
        this.setState({grid: response.data.grid})
    }

    renderGrid() {
        return (
            <div className={'ui grid'}>
                {this.state.grid.map(line => {
                    return (
                        this.renderLine(line)
                    )
                })}
            </div>)
    }

    renderLine(line) {
        return (
            <div className={"row"}>
                {line.map(cell => {
                    return this.renderCell(cell)
                })}
            </div>
        )
    }

    renderCell(cell) {
        return (<Cell icon={cell === 1 ? "alive" : "dead"}/>)
    }

    getNextGrid = () => {
        axios.post('http://localhost:5000/get_next_grid',
            {grid: this.state.grid}).then(response => {
            this.setState({grid: response.data.grid})
        })
    }

    getInitialGrid = () => {
        axios.get('http://localhost:5000/get_initial_grid').then(
            response => this.setState({grid: response.data.grid}))

    }

    render() {
        return (
            <div>
                <div className={"ui container"}>
                    {this.renderGrid()}
                    <div className={'ui grid'}>
                        <div className={"row"}>
                            <button className={"ui basic primary button"}
                                    style={styledButton}
                                    onClick={this.getNextGrid}
                            >
                                Next grid
                            </button>
                            <button className={"ui basic secondary button"}
                                    style={styledButton}
                                    onClick={this.getInitialGrid}
                            >
                                Initial grid
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        );
    }

}

export default Grid;