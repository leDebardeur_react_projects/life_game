import React, {Component} from 'react';
import deadIcon from '../assets/skull.png'
import aliveIcon from '../assets/alive.png'
const cellStyle = {
    width : "50px",
    height: "50px"
}
class Cell extends Component {
    renderSource(){
        return this.props.icon === "alive"? aliveIcon : deadIcon
    }
    render() {
        return (
              <img src={this.renderSource()} style={cellStyle} />
        );
    }
}

export default Cell;