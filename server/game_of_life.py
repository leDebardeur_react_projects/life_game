def parse_grid(path_to_grid):
    with open(path_to_grid, 'r') as f:
        raw_grid = f.read()
        grid = [[convert_char_into_int(char) for char in line]
                for line in raw_grid.split('\n')]
    return grid


def convert_char_into_int(char):
    if char == '.':
        int_to_return = 0
    else:
        int_to_return = 1
    return int_to_return


def get_next_grid(current_grid):
    new_grid = []
    for row, line in enumerate(current_grid):
        new_line = []
        for column, cell in enumerate(line):
            cell = get_next_cell_state(row, column, current_grid)
            new_line.append(cell)
        new_grid.append(new_line)

    return new_grid


def get_next_cell_state(row, column, current_grid):
    sum_neighbours = count_neighbours(row, column, current_grid)
    cell = current_grid[row][column]
    if cell == 1 and (sum_neighbours < 2 or sum_neighbours > 3):
        cell = 0
    elif cell == 0 and sum_neighbours == 3:
        cell = 1
    return cell


def count_neighbours(row, column, grid):
    current_grid = Grid(grid)
    sum_neighbours_row = current_grid.get(row, column - 1) + current_grid.get(row, column + 1)
    sum_neighbours_column = current_grid.get(row - 1, column) + current_grid.get(row + 1, column)
    sum_neighbours_diagonal = current_grid.get(row - 1, column - 1) + current_grid.get(row + 1, column + 1) + \
                              current_grid.get(row - 1, column + 1) + current_grid.get(row + 1, column - 1)
    sum_neighbours = sum_neighbours_column + sum_neighbours_row + sum_neighbours_diagonal
    return sum_neighbours


class Grid:

    def __init__(self, grid):
        self._grid = grid
        self.n_rows = len(grid)
        self.n_cols = len(grid[0])

    def get(self, row, column):
        if row < 0 or row >= self.n_rows:
            return 0
        if column < 0 or column >= self.n_cols:
            return 0
        else:
            return self._grid[row][column]