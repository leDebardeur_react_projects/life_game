from flask import Flask, request
from flask import jsonify
from flask_cors import CORS
from game_of_life import *

app = Flask(__name__)
CORS(app)

def inverse_grid(grid):
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            if grid[i][j] == 0:
                grid[i][j] = 1
            else :
                grid[i][j] = 0
    return grid
@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/get_initial_grid')
def get_initial_grid():
    initial_grid = parse_grid('initial_grid.txt')
    return jsonify({
        'grid': initial_grid
    })

@app.route('/get_next_grid', methods=['POST'])
def display_next_grid():
    next_grid = get_next_grid(request.json.get('grid'))
    return jsonify({"grid": next_grid})

if __name__ == '__main__':
    app.run(port=5000)
